#include <iostream>
#include <vector>
#include <utility>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <tuple>
#include <unordered_map>
#include <limits>
#include <cmath>
#include <fstream>
#include <cassert>

auto GenerateConnexions(const int inNbNodes = 5){
    srand48(0);
    const double treshold = 0.7;
    std::vector<std::pair<int,int>> edges;

    for(int idxSrc = 0 ; idxSrc < inNbNodes ; ++idxSrc){
        for(int idxDest = 0; idxDest < inNbNodes ; ++idxDest){
            if(idxSrc != idxDest && drand48() > treshold){
                edges.emplace_back(std::pair<int,int>(idxSrc, idxDest));
            }
        }
    }

    return edges;
}

auto ComputePositions(const std::vector<std::pair<int,int>>& inEdges){
    int nbNodes = 0;
    for(auto& edge : inEdges){
        nbNodes = std::max(std::max(nbNodes, edge.first), edge.second);
    }
    nbNodes += 1;

    // TODO find out good positions
    srand48(nbNodes);

    std::vector<std::pair<int,int>> positions(nbNodes);
    for(auto& position : positions){
        position.first = int(drand48()*1000);
        position.second = int(drand48()*1000);
    }
    // END TODO

    return positions;
}

auto FindBestIntersection(const int x1, const int y1, const int x2, const int y2,
                          const int width, const int height){
    // https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
    const int segments[4][4] = {{x1-width/2, y1-height/2, x1+width/2, y1-height/2},
                           {x1-width/2, y1-height/2, x1-width/2, y1+height/2},
                           {x1-width/2, y1+height/2, x1+width/2, y1+height/2},
                           {x1+width/2, y1-height/2, x1+width/2, y1+height/2}};

    for(int idxSegment = 0 ; idxSegment < 4 ; ++idxSegment){
        const int x3 = segments[idxSegment][0];
        const int y3 = segments[idxSegment][1];
        const int x4 = segments[idxSegment][2];
        const int y4 = segments[idxSegment][3];

        const double t = double((x1-x3)*(y3-y4) - (y1-y3)*(x3-x4))/double((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4));
        const double u = -double((x1-x2)*(y1-y3) - (y1-y2)*(x1-x3))/double((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4));

        if(0 <= t && t <= 1 && 0 <= u && u <= 1){
            const int px = (x1 +int(t * double(x2-x1)));
            const int py = (y1 +int(t * double(y2-y1)));
            return std::make_tuple(px,py);
        }
    }

    // Should not happen
    assert(0);
    return std::make_tuple(-1,-1);
}


void Draw(const std::vector<std::string>& inNodeNames,
          const std::vector<std::pair<int,int>>& inPositions,
          const std::vector<std::pair<int,int>>& inEdges){

    const std::string outputFilename = "graph.svg";
    std::ofstream svgfile(outputFilename);

    if(svgfile.is_open() == false){
        throw std::invalid_argument("Cannot open filename : " + outputFilename);
    }

    int vmin = std::numeric_limits<int>::max();
    int hmin = std::numeric_limits<int>::max();
    int vmax = std::numeric_limits<int>::min();
    int hmax = std::numeric_limits<int>::min();

    for(auto& position : inPositions){
        hmin = std::min(hmin, position.first);
        hmax = std::max(hmax, position.first);
        vmin = std::min(vmin, position.second);
        vmax = std::max(vmax, position.second);
    }

    const int margin = 50;
    const int hdim = (hmax - hmin) + margin*2;
    const int vdim = (vmax - vmin) + margin*2;

    svgfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    svgfile << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"" << hdim << "\" height=\"" << vdim << "\">\n";
    svgfile << "  <title>Execution trace</title>\n";
    svgfile << "  <desc>\n";
    svgfile << "    Call graph\n";
    svgfile << "  </desc>\n";
    svgfile << "\n";
    // Back
    svgfile << "  <rect width=\"" << hdim << "\" height=\"" << vdim << "\" x=\"0\" y=\"0\" fill=\"white\" />\n";

    std::unordered_map<int, std::tuple<int,int,int,int>> idToPosition;

    for(int idxNode = 0 ; idxNode < int(inNodeNames.size()) ; ++idxNode){
        const int rect_width = int(inNodeNames[idxNode].size())*15;
        const int rect_height = 40;
        const int xpos_start = inPositions[idxNode].first - hmin + margin - rect_width/2;
        const int ypos_start = inPositions[idxNode].second - vmin + margin - rect_height/2;

        idToPosition[idxNode] = std::tuple<int,int,int,int>{inPositions[idxNode].first - hmin + margin,
                                                    inPositions[idxNode].second - vmin + margin,
                                                    rect_width,
                                                    rect_height};

        svgfile << "<g>\n";
        svgfile << "    <title id=\"" << idxNode << "\">" << inNodeNames[idxNode] << "</title>\n";

        svgfile << "    <rect width=\"" << rect_width << "\" height=\"" << rect_height
                << "\" x=\"" << xpos_start << "\" y=\"" << ypos_start
                << "\" style=\"fill:" << "rgb(200,200,200)" << ";stroke:black" << ";stroke-width:" << 2 << "\" />\n";

        svgfile << "</g>\n";

        svgfile << "<text x=\"" << xpos_start << "\" y=\"" << ypos_start+25 <<
                   "\" font-size=\"30\" fill=\"black\">" << inNodeNames[idxNode] << "</text>\n";
    }

    svgfile << "<defs>\n"
               "<marker id=\"arrow\" markerWidth=\"20\" markerHeight=\"20\" refX=\"20\" refY=\"6\" orient=\"auto\" markerUnits=\"strokeWidth\">\n"
               "<path d=\"M0,0 L0,13 L20,6 z\" fill=\"gray\" />\n"
               "</marker>\n"
               "</defs>\n";

    for(const auto& edge : inEdges){
        int xpSrc,ypSrc;
        std::tie(xpSrc, ypSrc) = FindBestIntersection(std::get<0>(idToPosition[edge.first]), std::get<1>(idToPosition[edge.first]),
                                                      std::get<0>(idToPosition[edge.second]), std::get<1>(idToPosition[edge.second]),
                                                      std::get<2>(idToPosition[edge.first]), std::get<3>(idToPosition[edge.first]));
        int xpDest,ypDest;
        std::tie(xpDest, ypDest) = FindBestIntersection(std::get<0>(idToPosition[edge.second]), std::get<1>(idToPosition[edge.second]),
                                                      std::get<0>(idToPosition[edge.first]), std::get<1>(idToPosition[edge.first]),
                                                      std::get<2>(idToPosition[edge.second]), std::get<3>(idToPosition[edge.second]));

        svgfile << "  <line x1=\"" << xpSrc  << "\" y1=\"" << ypSrc
                << "\" x2=\"" << xpDest << "\" y2=\"" << ypDest << "\" stroke=\"" << "gray" <<"\" stroke-width=\"" << 1 <<"\"  marker-end=\"url(#arrow)\" />\n";
    }

    svgfile << "</svg>\n";
}

int main(int argc, char** argv){
    std::vector<std::pair<int,int>> edges;

    if(argc > 1){
        for(int idx = 1 ; idx < argc ; ++idx){
            int dest, src;
            if(sscanf(argv[idx], "%d->%d", &src, &dest) == 2){
                edges.emplace_back(std::pair<int,int>(src, dest));
            }
            else{
                std::cerr << "Error invalid parameter " << argv[idx] << std::endl;
            }
        }
    }
    else{
        edges = GenerateConnexions();
    }

    auto positions = ComputePositions(edges);

    std::vector<std::string> nodeNames(positions.size());
    for(int idxNode = 0 ; idxNode < int(positions.size()) ; ++idxNode){
        nodeNames[idxNode] = std::string("Node ").append(std::to_string(idxNode));
    }

    Draw(nodeNames, positions, edges);

    return 0;
}
